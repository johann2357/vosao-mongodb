package com.soft2.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import com.soft2.entity.PageEntity;
import com.soft2.entity.StructureEntity;
import com.soft2.dao.StructureDao;
import com.soft2.dao.PageDao;

@Service
public class StructureService {
	
	@Inject
	StructureDao structureDao;
	
	@Inject
	PageDao pageDao;
	
	@Inject
	I18nService i18n;
	
	public List<String> validateBeforeUpdate(final StructureEntity entity) {
		List<String> errors = new ArrayList<String>();
		StructureEntity foundStructure = structureDao.getByTitle(
				entity.getTitle());
		if (entity.getId() == null) {
			if (foundStructure != null) {
				errors.add(i18n.get("structure.already_exists"));
			}
		}
		else {
			if (foundStructure != null 
				&& !foundStructure.getId().equals(entity.getId())) {
				errors.add(i18n.get("structure.already_exists"));
			}
		}
		if (StringUtils.isEmpty(entity.getTitle())) {
			errors.add(i18n.get("title_is_empty"));
		}
		try {
			Document document = DocumentHelper.parseText(entity.getContent());
			if (!document.getRootElement().getName().equals("structure")) {
				errors.add(i18n.get("structure.invalid_xml"));
			}
		} catch (DocumentException e) {
			errors.add(i18n.get("parsing_error") + " " + e.getMessage());
		}
		return errors;
	}

	public List<String> remove(List<String> ids) {
		List<String> result = new ArrayList<String>();
		for (String id : ids) {
			StructureEntity structure = structureDao.getById(id);
			if (structure == null) {
				continue;
			}
			List<PageEntity> pages = pageDao.selectByStructure(id);
			if (pages.size() > 0) {
				result.add(i18n.get("structure.has_references", 
						structure.getTitle(), pages.get(0).getFriendlyURL()));
			}
			else {
				structureDao.remove(id);
			}
		}	
		return result;
	}

}
