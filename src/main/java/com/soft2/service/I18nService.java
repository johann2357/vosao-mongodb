package com.soft2.service;

import javax.inject.Inject;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
public class I18nService {

	@Inject
	MessageSource messageSource;
	
	@Inject
	VosaoContextService vosaoContext;

	public String get(String code) {
		return messageSource.getMessage(code, null, code, vosaoContext.getLocale());
	}

	public String get(String code, Object ...args) {
		return messageSource.getMessage(code, args, code, vosaoContext.getLocale());
	}
}
