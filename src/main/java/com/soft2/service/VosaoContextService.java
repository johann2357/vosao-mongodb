package com.soft2.service;

import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.soft2.dao.ConfigDao;
import com.soft2.entity.ConfigEntity;
import com.soft2.entity.UserEntity;

@Service
public class VosaoContextService {
	
	
	@Inject
	ConfigDao configDao;
	
	private ConfigEntity config;
	private UserEntity user;
	private List<String> skipURLs;
	private SystemService systemService;
	
	public Locale[] getSupportedLocales() {
		return new Locale[]{Locale.ENGLISH, new Locale("es"), new Locale("pt")};
	}

	public Locale getDefaultLocale() {
		return Locale.ENGLISH;
	}

	//TODO: Locale should be configured
	public Locale getLocale() {
		return getDefaultLocale();
	}
	
	private static ThreadLocal<VosaoContextService> threadInstance;
	
	public static VosaoContextService getInstance() {
		if (threadInstance == null) {
			threadInstance = new ThreadLocal<VosaoContextService>() {
				@Override
				protected VosaoContextService initialValue() {
					return new VosaoContextService();
				}
			};
		}
		return threadInstance.get();
	}
	
	public ConfigEntity getConfig() {
		if (config == null) {
			config = configDao.getConfig();
			if (config == null) {
				config = new ConfigEntity();
			}
		}
		return config;
	}

	public void setConfig(ConfigEntity config) {
		this.config = config;
	}

	public String getLanguage() {
		// TODO Auto-generated method stub
		return null;
	}

	public TimeZone getTimeZone() {
		if (user != null && !StringUtils.isEmpty(user.getTimezone())) {
			return TimeZone.getTimeZone(user.getTimezone());
		}
		ConfigEntity config = VosaoContextService.getInstance().getConfig();
		if (!StringUtils.isEmpty(config.getDefaultTimezone())) {
			return TimeZone.getTimeZone(config.getDefaultTimezone());
		}
		return TimeZone.getDefault();
	}

	public UserEntity getUser() {
		return user;
	}

	public boolean isSkipUrl(final String url) {
    	for (String u : skipURLs) {
    		if (url.startsWith(u)) {
    			return true;
    		}
    	}
    	return false;
    }
	
	public SystemService getSystemService() {
		if (systemService == null) {
			systemService = new SystemService();
		}
		return systemService;
	}
}
