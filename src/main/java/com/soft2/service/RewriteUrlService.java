package com.soft2.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class RewriteUrlService {

	private Map<String,String> rules = new HashMap<String, String>();
	
	public void addRule(String from, String to) {
		rules.put(from, to);
	}

	public String rewrite(String url) {
		String newUrl = null;
		for (String from : rules.keySet()) {
			if (url.matches(from)) {
				newUrl = url.replaceAll(from, rules.get(from));
			}
		}
		return newUrl != null ? newUrl : url;
	}

	public void addRules(Map<String, String> rules) {
		this.rules.putAll(rules);
	}

	public void removeRules(Map<String, String> rules) {
		for (String key : rules.keySet()) {
			this.rules.remove(key);
		}
	}
}
