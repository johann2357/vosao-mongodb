package com.soft2.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class SystemService {
	
	private CacheService cache;
	private PageCacheService pageCache;
//	private VelocityEngine velocityEngine;
	
	static Logger logger = LoggerFactory.getLogger(SystemService.class);

	public CacheService getCache() {
		if (cache == null) {
			cache = new CacheService();
		}
		return cache;
	}

	public PageCacheService getPageCache() {
		if (pageCache == null) {
			pageCache = new PageCacheService();
		}
		return pageCache;
	}
	
//	public VelocityEngine getVelocityEngine() {
//		if (velocityEngine == null) {
//	        try {
//	            velocityEngine = new VelocityEngine("WEB-INF/velocity.properties");
//				velocityEngine.init();
//			} catch (Exception e) {
//	            logger.error("Can't init velocity engine. " + e.getMessage());
//			}
//		}
//		return velocityEngine;
//	}
//
//	public String render(String template, VelocityContext context) {
//		StringWriter wr = new StringWriter();
//		String log = "vm";
//		try {
//			getVelocityEngine().evaluate(context, wr, log, template);
//			return wr.toString();
//		} catch (ParseErrorException e) {
//			return e.toString();
//		} catch (MethodInvocationException e) {
//			return e.toString();
//		} catch (ResourceNotFoundException e) {
//			return e.toString();
//		} catch (IOException e) {
//			return e.toString();
//		}
//	}
	

}
