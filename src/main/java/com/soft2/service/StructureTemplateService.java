package com.soft2.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.soft2.dao.PageDao;
import com.soft2.dao.StructureTemplateDao;
import com.soft2.entity.PageEntity;
import com.soft2.entity.StructureTemplateEntity;

@Service
public class StructureTemplateService {
	
	@Inject
	StructureTemplateDao structureTemplateDao;
	
	@Inject
	PageDao pageDao;
	
	@Inject
	I18nService i18n;
	
	@Inject
	VosaoContextService vosaoContextService;

	public List<String> validateBeforeUpdate(
			final StructureTemplateEntity entity) {
		List<String> errors = new ArrayList<String>();
		StructureTemplateEntity foundStructure = structureTemplateDao
				.getByName(entity.getName());
		if (entity.getId() == null) {
			if (foundStructure != null) {
				errors.add(i18n.get("structureTemplate.already_exists"));
			}
		}
		else {
			if (foundStructure != null 
				&& !foundStructure.getId().equals(entity.getId())) {
				errors.add(i18n.get("structureTemplate.already_exists"));
			}
		}
		if (StringUtils.isEmpty(entity.getTitle())) {
			errors.add(i18n.get("title_is_empty"));
		}
		if (StringUtils.isEmpty(entity.getName())) {
			errors.add(i18n.get("name_is_empty"));
		}
		return errors;
	}
	
	public List<String> remove(List<String> ids) {
		List<String> result = new ArrayList<String>();
		for (String id : ids) {
			StructureTemplateEntity entity = structureTemplateDao
					.getById(id);
			if (entity == null) {
				continue;
			}
			List<PageEntity> pages = pageDao
					.selectByStructureTemplate(id);
			if (pages.size() > 0) {
				result.add(i18n.get("structureTemplate.has_references", 
						entity.getTitle(), pages.get(0).getFriendlyURL()));
			}
			else {
				structureTemplateDao.removeById(id);
			}
		}	
		return result;
	}

	public StructureTemplateEntity save(StructureTemplateEntity template) {
		Set<String> pages = new HashSet<String>();
		for (PageEntity page : pageDao.selectByStructureTemplate(
				template.getId())) {
			pages.add(page.getFriendlyURL());
		}
		for (String url : pages) {
			vosaoContextService.getSystemService().getPageCache().remove(url);
		}
		return structureTemplateDao.insert(template);
	}
	
}
