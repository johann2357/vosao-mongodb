package com.soft2.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.soft2.entity.CommentEntity;
import com.soft2.entity.PageEntity;
import com.soft2.service.CommentService;

@RestController
public class CommentController {
	
	@Inject
	CommentService service;
	
	@RequestMapping(value = "/comment/", method = RequestMethod.GET)
	public List<CommentEntity> listAll() {
		return service.getAll();
	}
	
	@RequestMapping(value = "/comment/", method = RequestMethod.POST)
	public String saveTag(@RequestBody CommentEntity tag) {
		return service.insert(tag);
	}
	
	@RequestMapping(value = "/comment/", method = RequestMethod.DELETE)
	public String remove(@RequestBody String id) {
		service.remove(id);
		return "Removed";
	}
	
	@RequestMapping(value = "/comment/addComment/", method = RequestMethod.GET)
	public CommentEntity addComment(@RequestBody String name,
			@RequestBody String content, @RequestBody PageEntity page) {
		return service.addComment(name, content, page);
	}
	
	@RequestMapping(value = "/comment/removeList/", method = RequestMethod.DELETE)
	public String removeList(@RequestBody List<String> ids) {
		service.remove(ids);
		return "Disable";
	}
	
	@RequestMapping(value = "/comment/disable/", method = RequestMethod.POST)
	public String disable(@RequestBody List<String> ids) {
		service.disable(ids);
		return "Disable";
	}
	
	@RequestMapping(value = "/comment/enable/", method = RequestMethod.POST)
	public String enable(@RequestBody List<String> ids) {
		service.enable(ids);
		return "Enable";
	}
	
}
