package com.soft2.controller;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.soft2.entity.MessageEntity;
import com.soft2.service.MessageService;

@RestController
public class MessageController {

	@Inject
	MessageService service;
	
	@RequestMapping(value = "/message/", method = RequestMethod.GET)
	public List<MessageEntity> listAll() {
		return service.getAll();
	}
	
	@RequestMapping(value = "/message/", method = RequestMethod.POST)
	public String saveTag(@RequestBody MessageEntity tag) {
		return service.insert(tag);
	}
	
	@RequestMapping(value = "/message/", method = RequestMethod.DELETE)
	public String remove(@RequestBody String id) {
		service.remove(id);
		return "Removed";
	}
	
	@RequestMapping(value = "/message/getBundle/", method = RequestMethod.GET)
	public Map<String, String> getBundle(@RequestBody String langCode) {
		return service.getBundle(langCode);
	}
}
