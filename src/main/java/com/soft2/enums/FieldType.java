package com.soft2.enums;

public enum FieldType {
	TEXT, CHECKBOX, RADIO, PASSWORD, LISTBOX, FILE;
}
