package com.soft2.enums;

public enum UserRole {
	USER, ADMIN, SITE_USER, GUEST;
}