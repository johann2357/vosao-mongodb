package com.soft2.entity;

import java.util.Date;
//import com.google.appengine.api.datastore.Key;


public class BaseEntityImpl implements BaseEntity {

	private String id;

	//private Key key;
	@SuppressWarnings("unused")
	private String createUserEmail;
	private Date createDate;
	@SuppressWarnings("unused")
	private String modUserEmail;
	@SuppressWarnings("unused")
	private Date modDate;
	
	public BaseEntityImpl() {
		createDate = new Date();
		modDate = createDate;
		createUserEmail = "";
		modUserEmail = "";
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isNew() {
		return id == null;
	}

	public void copy(BaseEntity entity) {
		// TODO Auto-generated method stub

	}

	public String getCreateUserEmail() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setCreateUserEmail(String createUserEmail) {
		// TODO Auto-generated method stub

	}

	public Date getCreateDate() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setCreateDate(Date createDate) {
		// TODO Auto-generated method stub

	}

	public String getModUserEmail() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setModUserEmail(String modUserEmail) {
		// TODO Auto-generated method stub

	}

	public Date getModDate() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setModDate(Date modDate) {
		// TODO Auto-generated method stub

	}

	public String getCreateDateString() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getModDateString() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCreateDateTimeString() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getModDateTimeString() {
		// TODO Auto-generated method stub
		return null;
	}

}
