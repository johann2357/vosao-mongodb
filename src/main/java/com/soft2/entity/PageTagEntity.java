package com.soft2.entity;

import java.util.List;

public class PageTagEntity extends BaseEntityImpl {

	private String pageURL;

	private List<String> tags;

	public PageTagEntity() {
	}

	public PageTagEntity(String pageURL) {
		this.pageURL = pageURL;
	}

	public String getPageURL() {
		return pageURL;
	}

	public void setPageURL(String pageURL) {
		this.pageURL = pageURL;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}
}
