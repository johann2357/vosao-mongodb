package com.soft2.entity;

import java.util.Date;
// TODO: find equivalent in MongoDB
//import com.google.appengine.api.datastore.Key;
//import com.google.appengine.api.datastore.Entity;

public interface BaseEntity {

	// Key getKey();

	// void setKey(Key key);

	String getId();

	void setId(String id);

	boolean isNew();

	/**
	 * Copy all fields from entity.
	 * 
	 * @param entity
	 *            - data to copy from.
	 */
	void copy(BaseEntity entity);

	// audit fields

	String getCreateUserEmail();

	void setCreateUserEmail(String createUserEmail);

	Date getCreateDate();

	void setCreateDate(Date createDate);

	String getModUserEmail();

	void setModUserEmail(String modUserEmail);

	Date getModDate();

	void setModDate(Date modDate);

	String getCreateDateString();

	String getModDateString();

	String getCreateDateTimeString();

	String getModDateTimeString();

}