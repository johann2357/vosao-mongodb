package com.soft2.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.soft2.service.vo.StructureFieldVO;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

public class StructureEntity extends BaseEntityImpl {

	Logger logger = LoggerFactory.getLogger(getClass());
	
	private String title;
	private String content;
	
	public StructureEntity() {
		content = "";
	}

	public StructureEntity(String title, String content) {
		this();
		this.title = title;
		this.content = content;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}

	public List<StructureFieldVO> getFields() {
		try {
			Document doc = DocumentHelper.parseText(getContent());
			List<StructureFieldVO> result = new ArrayList<StructureFieldVO>();
			for (@SuppressWarnings("unchecked")
			Iterator<Element> i = doc.getRootElement().elementIterator(); 
					i.hasNext(); ) {
					Element element = i.next();
					if (element.getName().equals("field")) {
						result.add(new StructureFieldVO(
								element.elementText("title"),
								element.elementText("name"),
								element.elementText("type")));
					}
			}
			return result;
		} catch (DocumentException e) {
			logger.error(e.getMessage());
			return Collections.emptyList();
		}
	}
}
