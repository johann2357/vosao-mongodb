package com.soft2.entity;


public class SeoUrlEntity extends BaseEntityImpl {
	private String fromLink;
	private String toLink;

	public SeoUrlEntity() {
	}

	public SeoUrlEntity(String aFrom, String aTo) {
		this();
		fromLink = aFrom;
		toLink = aTo;
	}
	
	public String getFromLink() {
		return fromLink;
	}
	
	public void setFromLink(String aFrom) {
		fromLink = aFrom;
	}

	public String getToLink() {
		return toLink;
	}

	public void setToLink(String to) {
		this.toLink = to;
	}
}
