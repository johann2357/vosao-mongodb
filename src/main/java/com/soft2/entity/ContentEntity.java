package com.soft2.entity;

public class ContentEntity extends BaseEntityImpl {

	
	private String parentClass;
	private String parentKey;
	private String languageCode;
	private String content;
	
	public ContentEntity() {
		content = "";
	}

	public ContentEntity(String parentClass, String pageId, 
			String languageCode, String content) {
		this();
		this.parentClass = parentClass;
		this.parentKey = pageId;
		this.languageCode = languageCode;
		setContent(content);
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getParentClass() {
		return parentClass;
	}

	public void setParentClass(String parentClass) {
		this.parentClass = parentClass;
	}

	public String getParentKey() {
		return parentKey;
	}

	public void setParentKey(String parentKey) {
		this.parentKey = parentKey;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
}
