package com.soft2.dao;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.soft2.entity.StructureTemplateEntity;

@Repository
public class StructureTemplateDao extends AbstractDao<StructureTemplateEntity> {
	
	@Inject
	MongoTemplate mongoTemplate;
	
	public StructureTemplateDao() {
		super();
	}
	
	public List<StructureTemplateEntity> selectByStructure(String structureId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("structureId").is(structureId));
		return mongoTemplate.find(query, StructureTemplateEntity.class);
	}

	public StructureTemplateEntity getByName(String name) {
		Query query = new Query();
		query.addCriteria(Criteria.where("name").is(name));
		return mongoTemplate.findOne(query, StructureTemplateEntity.class);
	}

	public StructureTemplateEntity getByTitle(String title) {
		Query query = new Query();
		query.addCriteria(Criteria.where("title").is(title));
		return mongoTemplate.findOne(query, StructureTemplateEntity.class);
	}
	
	protected Class<StructureTemplateEntity> getCollectionClass() {
		return StructureTemplateEntity.class;
	}

}
