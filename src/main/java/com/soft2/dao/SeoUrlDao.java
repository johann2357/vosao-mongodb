package com.soft2.dao;

import javax.inject.Inject;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.soft2.entity.SeoUrlEntity;

@Repository
public class SeoUrlDao extends AbstractDao<SeoUrlEntity> {
	
	@Inject
	MongoTemplate mongoTemplate;
	
	public SeoUrlEntity save(SeoUrlEntity obj) {
		mongoTemplate.save(obj);
		return obj;
	}

	public SeoUrlDao() {
		super();
	}

	public SeoUrlEntity getByFrom(final String from) {
		Query query = new Query();
		query.addCriteria(Criteria.where("fromLink").is(from));
		return mongoTemplate.findOne(query, SeoUrlEntity.class);
	}
	
	@Override
	protected Class<SeoUrlEntity> getCollectionClass() {
		return SeoUrlEntity.class;
	}
}
