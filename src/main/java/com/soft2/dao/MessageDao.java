package com.soft2.dao;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.soft2.entity.MessageEntity;

@Repository
public class MessageDao extends AbstractDao<MessageEntity> {

	@Inject
	MongoTemplate mongoTemplate;

	public MessageEntity save(MessageEntity message) {
		mongoTemplate.save(message);
		return message;
	}

	public MessageDao() {
		super();
	}
	
	public List<MessageEntity> selectByCode(String code) {
		Query query = new Query();
		query.addCriteria(Criteria.where("code").is(code));
		return mongoTemplate.find(query, MessageEntity.class);
	}
	
	public MessageEntity getByCode(String code, String languageCode) {
		Query query = new Query();
		query.addCriteria(Criteria.where("code").is(code).andOperator(Criteria.where("languageCode").is(languageCode)));
		return mongoTemplate.findOne(query, MessageEntity.class);
	}

	public List<MessageEntity> select(String languageCode) {
		Query query = new Query();
		query.addCriteria(Criteria.where("languageCode").is(languageCode));
		return mongoTemplate.find(query, MessageEntity.class);
	}

	@Override
	protected Class<MessageEntity> getCollectionClass() {
		return MessageEntity.class;
	}

}
