//package com.soft2.dao;
//
//import com.mongodb.DBCollection;
//
//public class DaoFactory {
//    
//    public static ExampleDao getExampleDao() {
//        return (ExampleDao)getDAOByClassAndName(ExampleDao.class, "example");
//    }
//    public static CommentDao getCommentDao() {
//        return (CommentDao)getDAOByClassAndName(CommentDao.class, "comment");
//    }
//    public static LanguageDao getLanguageDao() {
//        return (LanguageDao)getDAOByClassAndName(LanguageDao.class, "language");
//    }
//    public static MessageDao getMessageDao() {
//        return (MessageDao)getDAOByClassAndName(MessageDao.class, "message");
//    }
//    public static SeoUrlDao getSeoUrlDao() {
//        return (SeoUrlDao)getDAOByClassAndName(SeoUrlDao.class, "seourl");
//    }
//    public static TagDao getTagDao() {
//        return (TagDao)getDAOByClassAndName(TagDao.class, "tag");
//    }
//    @SuppressWarnings("rawtypes")
//	public static AbstractDao getDAOByClassAndName(Class c, String name) {
//        try 
//        {
//            DBCollection collection= getCollection(name);
//            AbstractDao d = (AbstractDao)c.newInstance();
//            d.setDbCollection(collection);       
//            return d;
//        } catch (InstantiationException ex) {
//            ex.printStackTrace();
//        } catch (IllegalAccessException ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//     public static DBCollection getCollection(String name)
//    {
//        return MongoDBUtil.getCollection(name);
//    }
//}