package com.soft2.dao;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.soft2.entity.TagEntity;

@Repository
public class TagDao extends AbstractDao<TagEntity> {

	@Inject
	MongoTemplate mongoTemplate;

	public TagEntity save(TagEntity obj) {
		mongoTemplate.save(obj);
		return obj;
	}
	
	public TagDao() {
		super();
	}
	
	public TagEntity getByName(String parent, String name) {
		Query query = new Query();
		query.addCriteria(Criteria.where("parent").is(parent).andOperator(
			Criteria.where("name").is(name)));
		return mongoTemplate.findOne(query, TagEntity.class);
	}

	public List<TagEntity> selectByParent(String parent) {
		Query query = new Query();
		query.addCriteria(Criteria.where("parent").is(parent));
		return mongoTemplate.find(query, TagEntity.class);
	}

	@Override
	protected Class<TagEntity> getCollectionClass() {
		return TagEntity.class;
	}
}
