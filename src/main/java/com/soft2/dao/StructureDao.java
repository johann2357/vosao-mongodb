package com.soft2.dao;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;


import com.soft2.entity.StructureEntity;
import com.soft2.entity.helper.StructureTemplateHelper;

@Repository
public class StructureDao extends AbstractDao<StructureEntity> {
	
	@Inject
	MongoTemplate mongoTemplate;
	
	@Inject
	StructureTemplateDao structureTemplateDao;
	
	public StructureDao() {
		super();
	}

	public StructureEntity getByTitle(String title) {
		Query query = new Query();
		query.addCriteria(Criteria.where("title").is(title));
		return mongoTemplate.findOne(query, StructureEntity.class);
	}
	
	public void remove(String id) {
		List<String> structureTemplateIds = StructureTemplateHelper.createIdList(
				structureTemplateDao.selectByStructure(id));
		structureTemplateDao.remove(structureTemplateIds);
		remove(id);
	}

	@Override
	public void remove(List<String> ids) {
		for (String id : ids) {
			remove(id);
		}
	}

	@Override
	protected Class<StructureEntity> getCollectionClass() {
		return StructureEntity.class;
	}
	
}
