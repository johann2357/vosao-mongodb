package com.soft2.dao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import com.soft2.dao.UserGroupDao;
import com.soft2.entity.UserGroupEntity;
import com.soft2.enums.UserRole;

import com.soft2.entity.UserEntity;

@Repository
public class UserDao extends AbstractDao<UserEntity> {
	
	@Inject
	MongoTemplate mongoTemplate;
	
	@Inject
	UserGroupDao userGroupDao;
	
	public UserDao() {
		super();
	}

	public UserEntity getByEmail(final String email) {
		Query query = new Query();
		query.addCriteria(Criteria.where("email").is(email));
		return mongoTemplate.findOne(query, UserEntity.class);
	}

	public List<UserEntity> getByRole(final UserRole role) {
		Query query = new Query();
		query.addCriteria(Criteria.where("role").is(role.name()));
		return mongoTemplate.find(query, UserEntity.class);
	}

	public List<UserEntity> selectByGroup(final String groupId) {
		List<UserGroupEntity> users = userGroupDao.selectByGroup(groupId);
		List<UserEntity> result = new ArrayList<UserEntity>();
		for (UserGroupEntity userGroup : users) {
			UserEntity user = getById(userGroup.getUserId());
			if (user != null) {
				result.add(user);
			}
		}
		return result;
	}

//	private UserGroupDao getUserGroupDao() {
//		return VosaoContextService.getInstance().getBusiness().getDao()
//				.getUserGroupDao();
//	}

	public UserEntity getByKey(String key) {
		if (key == null) {
			return null;
		}
		Query query = new Query();
		query.addCriteria(Criteria.where("forgotPasswordKey").is(key));
		return mongoTemplate.findOne(query, UserEntity.class);
	}
	
	
	@Override
	protected Class<UserEntity> getCollectionClass() {
		return UserEntity.class;
	}

}
