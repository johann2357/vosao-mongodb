package com.soft2.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.soft2.config.ApplicationConfig;
import com.soft2.config.MongoConfig;
import com.soft2.dao.StructureDao;
import com.soft2.dao.StructureTemplateDao;
import com.soft2.entity.StructureEntity;
import com.soft2.entity.StructureTemplateEntity;
import com.soft2.enums.StructureTemplateType;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationConfig.class, MongoConfig.class })
public class StructureTemplateServiceTest {

	@Inject
	StructureTemplateService structureTemplateService;
	
	@Inject
	StructureDao structureDao;
	
	@Inject
	StructureTemplateDao structureTemplateDao;
	
	List<String> structures = new ArrayList<String>();
	
	List<StructureTemplateEntity> structureTemplates = new ArrayList<StructureTemplateEntity>();
	
	private StructureTemplateEntity addStructureTemplate(String title){
	    StructureTemplateEntity ent = new StructureTemplateEntity(
	    	title, title, title, StructureTemplateType.VELOCITY, title);
	    structureTemplates.add(ent);
	    return structureTemplateDao.save(ent);
	}
	
	
	@Before
	public void setupData() {
	}
	
	@After
	public void tearDown() {
		//TODO: eliminar en una sola operacion en batch.
		for (StructureTemplateEntity obj : structureTemplates) {
			structureTemplateDao.remove(obj);
		}
	}
	
	@Test
	public void testValidateBeforeUpdate() {
		List<String> result = 
			structureTemplateService.validateBeforeUpdate(
				addStructureTemplate("test6"));
		Assert.assertEquals(0, result.size());
		result = structureTemplateService.validateBeforeUpdate(
				new StructureTemplateEntity());
		Assert.assertEquals(3, result.size());
		StructureTemplateEntity en = new StructureTemplateEntity();
		en.setId("test7");
		result = structureTemplateService.validateBeforeUpdate(en);
		Assert.assertEquals(3, result.size());
	}
	
	@Test
	public void testRemove() {
		StructureTemplateEntity en = new StructureTemplateEntity();
		en.setId("test2");
		structures.add(en.getId());
		List<String> result = structureTemplateService.remove(structures);
		Assert.assertEquals(0, result.size());

	}
	
	@Test
	public void testSave() {
		StructureTemplateEntity en = new StructureTemplateEntity();
		StructureTemplateEntity en2 = structureTemplateService.save(en);
		Assert.assertEquals(en, en2);
	}

}
