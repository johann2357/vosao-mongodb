package com.soft2.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.soft2.config.ApplicationConfig;
import com.soft2.config.MongoConfig;
import com.soft2.dao.StructureDao;
import com.soft2.entity.StructureEntity;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationConfig.class, MongoConfig.class })
public class StructureServiceTest {

	@Inject
	StructureService structureService;
	
	@Inject
	StructureDao structureDao;
	
	List<String> structures = new ArrayList<String>();
	
	@Before
	public void setupData() {
	}
	
	@After
	public void tearDown() {

	}
	
	@Test
	public void testValidateBeforeUpdate() {
		List<String> result = 
			structureService.validateBeforeUpdate(
				new StructureEntity("sitemap", "file1"));
		Assert.assertEquals(1, result.size());
		result = structureService.validateBeforeUpdate(
					new StructureEntity());
		Assert.assertEquals(2, result.size());
		StructureEntity en = new StructureEntity();
		en.setId("test");
		result = structureService.validateBeforeUpdate(en);
		Assert.assertEquals(2, result.size());
	}
	
	@Test
	public void testRemove() {
		StructureEntity en = new StructureEntity();
		en.setId("test");
		structures.add(en.getId());
		List<String> result = structureService.remove(structures);
		Assert.assertEquals(0, result.size());
		
	}

}
