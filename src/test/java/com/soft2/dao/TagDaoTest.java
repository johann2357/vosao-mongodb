package com.soft2.dao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.soft2.config.ApplicationConfig;
import com.soft2.config.MongoConfig;
import com.soft2.entity.TagEntity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationConfig.class, MongoConfig.class })
public class TagDaoTest {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Inject
	TagDao dao;
	
	List<TagEntity> tags = new ArrayList<TagEntity>();
	
	private TagEntity addTag(String parent, String name){
	    TagEntity ent = new TagEntity(parent, name, name);
	    tags.add(ent);
	    return dao.save(ent);
	}
	
	@Before
	public void setupData() {
		addTag(null, "test1");
		addTag("1L", "test2");
		addTag("2L", "test3");
	}
	
	@After
	public void tearDown() {
		//TODO: eliminar en una sola operacion en batch.
		for (TagEntity obj : tags) {
			dao.remove(obj);
		}
	}
	
	@Test
	public void testSave() {
		TagEntity obj = new TagEntity();
		dao.save(obj);
		logger.debug("Persist Tag ID: " + obj.getId());
		Assert.assertNotNull(obj.getId());
		tags.add(obj);
	}

	@Test
	public void testGetByName() {
		TagEntity tag = dao.getByName(null, "test1");
		Assert.assertNotNull(tag);
		Assert.assertEquals("test1", tag.getName());
	}	
	
	@Test
	public void testSelectByParent() {
		List<TagEntity> tags =  dao.selectByParent(null);
		Assert.assertNotNull(tags);;
		Assert.assertEquals(1, tags.size());
		Assert.assertEquals("test1", tags.get(0).getName());
	}
	
	@Test
	public void testGetCollectionClass() {
		Assert.assertSame(TagEntity.class, dao.getCollectionClass());
	}

}
