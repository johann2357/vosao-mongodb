package com.soft2.dao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.soft2.config.ApplicationConfig;
import com.soft2.config.MongoConfig;
import com.soft2.entity.SeoUrlEntity;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationConfig.class, MongoConfig.class })
public class SeoUrlDaoTest {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Inject
	SeoUrlDao dao;
	
	List<SeoUrlEntity> seourls = new ArrayList<SeoUrlEntity>();
	
	private SeoUrlEntity addSeoUrl(String from, String to) {
	    SeoUrlEntity obj = new SeoUrlEntity(from, to);   
	    seourls.add(obj);
	    return dao.save(obj);
	}
	
	@Before
	public void setupData() {
		addSeoUrl("/man", "/woman");
		addSeoUrl("/black", "/white");
		addSeoUrl("/super/man", "/ordinal/woman");
	}
	
	@After
	public void tearDown() {
		//TODO: eliminar en una sola operacion en batch.
		for (SeoUrlEntity obj : seourls) {
			dao.remove(obj);
		}
	}
	
	@Test
	public void testSave() {
		SeoUrlEntity obj = new SeoUrlEntity();
		dao.save(obj);
		logger.debug("Persist SeoUrl ID: " + obj.getId());
		Assert.assertNotNull(obj.getId());
		seourls.add(obj);
	}
    
	@Test
	public void testGetByFrom() {
		addSeoUrl("/man", "/woman");
		addSeoUrl("/black", "/white");
		addSeoUrl("/super/man", "/ordinal/woman");
		SeoUrlEntity s = dao.getByFrom("/man");
		Assert.assertNotNull(s);
		Assert.assertEquals("/woman", s.getToLink());
		s = dao.getByFrom(null);
		Assert.assertNull(s);
		s = dao.getByFrom("/megahit");
		Assert.assertNull(s);
	}	
	
	@Test
	public void testGetCollectionClass() {
		Assert.assertSame(SeoUrlEntity.class, dao.getCollectionClass());
	}
}
