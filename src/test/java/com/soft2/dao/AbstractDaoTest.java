package com.soft2.dao;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.soft2.config.ApplicationConfig;
import com.soft2.config.MongoConfig;
import com.soft2.entity.TagEntity;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationConfig.class, MongoConfig.class })
public class AbstractDaoTest {

	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Inject
	AbstractDao<TagEntity> tagDao;
	
	
	@Test
	public void testNotNull() {
		Assert.assertNotNull(tagDao.getDbCollection());
		Assert.assertNotNull(tagDao.getCollectionName());
	}
	
	@Test
	public void testNull() {
		Assert.assertNull(tagDao.getById("test"));
	}

}
