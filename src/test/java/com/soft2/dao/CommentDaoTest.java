package com.soft2.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.soft2.config.ApplicationConfig;
import com.soft2.config.MongoConfig;
import com.soft2.entity.CommentEntity;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationConfig.class, MongoConfig.class })
public class CommentDaoTest {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Inject
	CommentDao dao;
	
	List<CommentEntity> comments = new ArrayList<CommentEntity>();
	List<String> ids = new ArrayList<String>();
	
	private CommentEntity addComment(String aPageUrl, Date published, Boolean disabled){
	    CommentEntity ent = new CommentEntity(
	    	aPageUrl, aPageUrl, published, aPageUrl, disabled);
	    comments.add(ent);
	    dao.save(ent);
	    ids.add(ent.getId());
	    return ent;
	}
	
	@Before
	public void setupData() {
		addComment("comment", new Date(1L), false);
		addComment("comment", new Date(2L), true);
		addComment("comment1", new Date(3L), true);
		addComment("comment1", new Date(4L), false);
		addComment("comment1", new Date(5L), true);
	
	}
	
	@After
	public void tearDown() {
		//TODO: eliminar en una sola operacion en batch.
		for (CommentEntity obj : comments) {
			dao.remove(obj);
		}
	}
	
	@Test
	public void testSave() {
		CommentEntity comment = new CommentEntity();
		dao.save(comment);
		logger.debug("Persist Comment ID: " + comment.getId());
		Assert.assertNotNull(comment.getId());
		comments.add(comment);
	}
	
	@Test
	public void testGetByPage() {
		List<CommentEntity> coms = dao.getByPage("comment");
		Assert.assertEquals(2, coms.size());
	}
	
	@Test
	public void testGetByPage2() {
		List<CommentEntity> coms = dao.getByPage("comment1", true);
		Assert.assertEquals(2, coms.size());
	}
	
	@Test
	public void testGetByPage3() {
		List<CommentEntity> coms = dao.getByPage("comment1", true, "DESC");
		Assert.assertTrue(coms.get(0).getPublishDate().after(coms.get(1).getPublishDate()));
		List<CommentEntity> coms2 = dao.getByPage("comment1", true, "ASC");
		Assert.assertTrue(coms2.get(0).getPublishDate().before(coms2.get(1).getPublishDate()));
	}
	
	@Test
	public void testGetRecent() {
		List<CommentEntity> coms = dao.getRecent(2);
		Assert.assertTrue(coms.get(0).getPublishDate().after(coms.get(1).getPublishDate()));
	}
	
	@Test
	public void testEnable() {
		Boolean disable = false;
		dao.enable(ids);
		List<CommentEntity> coms = dao.getByPage("comment", disable);
		Assert.assertEquals(2, coms.size());
	}
	
	@Test
	public void testDisable() {
		Boolean disable = true;
		dao.disable(ids);
		List<CommentEntity> coms = dao.getByPage("comment", disable);
		Assert.assertEquals(2, coms.size());
	}
	
	@Test
	public void testRemoveByPage() {
	    dao.removeByPage("comment");
	    List<CommentEntity> coms = dao.getByPage("comment");
	    Assert.assertEquals(0, coms.size());
	    List<CommentEntity> coms1 = dao.getAll();
	    Assert.assertEquals(3, coms1.size());
	}
	
	@Test
	public void testGetCollectionClass() {
		Assert.assertSame(CommentEntity.class, dao.getCollectionClass());
	}
}
