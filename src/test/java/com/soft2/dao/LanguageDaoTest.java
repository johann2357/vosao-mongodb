package com.soft2.dao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.soft2.config.ApplicationConfig;
import com.soft2.config.MongoConfig;
import com.soft2.entity.LanguageEntity;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationConfig.class, MongoConfig.class })
public class LanguageDaoTest {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Inject
	LanguageDao dao;
	
	List<LanguageEntity> languages = new ArrayList<LanguageEntity>();
	
	private LanguageEntity addLanguage(String code) {
	    LanguageEntity obj = new LanguageEntity(code, code);   
	    languages.add(obj);
	    return dao.save(obj);
	}
	
	@Before
	public void setupData() {
		addLanguage("en");
		addLanguage("ru");
		addLanguage("uk");
		addLanguage("fr");
	}
	
	@After
	public void tearDown() {
		//TODO: eliminar en una sola operacion en batch.
		for (LanguageEntity obj : languages) {
			dao.remove(obj);
		}
	}
	
	@Test
	public void testSave() {
		LanguageEntity obj = new LanguageEntity();
		dao.save(obj);
		logger.debug("Persist Language ID: " + obj.getId());
		Assert.assertNotNull(obj.getId());
		languages.add(obj);
	}
	
	@Test
	public void testGetByCode() {
		LanguageEntity l = dao.getByCode("eng");
		Assert.assertNull(l);
		l = dao.getByCode(null);
		Assert.assertNull(l);
		l = dao.getByCode("en");
		Assert.assertNotNull(l);
		Assert.assertEquals("en", l.getCode());
	}
	
	@Test
	public void testGetCollectionClass() {
		Assert.assertSame(LanguageEntity.class, dao.getCollectionClass());
	}
}
